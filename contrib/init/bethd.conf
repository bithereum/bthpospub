description "Bithereum Daemon"

start on runlevel [2345]
stop on starting rc RUNLEVEL=[016]

env BETHD_BIN="/usr/bin/bethd"
env BETHD_USER="bitcoin"
env BETHD_GROUP="bitcoin"
env BETHD_PIDDIR="/var/run/bethd"
# upstart can't handle variables constructed with other variables
env BETHD_PIDFILE="/var/run/bethd/bethd.pid"
env BETHD_CONFIGFILE="/etc/bitcoin/bithereum.conf"
env BETHD_DATADIR="/var/lib/bethd"

expect fork

respawn
respawn limit 5 120
kill timeout 60

pre-start script
    # this will catch non-existent config files
    # bethd will check and exit with this very warning, but it can do so
    # long after forking, leaving upstart to think everything started fine.
    # since this is a commonly encountered case on install, just check and
    # warn here.
    if ! grep -qs '^rpcpassword=' "$BETHD_CONFIGFILE" ; then
        echo "ERROR: You must set a secure rpcpassword to run bethd."
        echo "The setting must appear in $BETHD_CONFIGFILE"
        echo
        echo "This password is security critical to securing wallets "
        echo "and must not be the same as the rpcuser setting."
        echo "You can generate a suitable random password using the following "
        echo "command from the shell:"
        echo
        echo "bash -c 'tr -dc a-zA-Z0-9 < /dev/urandom | head -c32 && echo'"
        echo
        echo "It is recommended that you also set alertnotify so you are "
        echo "notified of problems:"
        echo
        echo "ie: alertnotify=echo %%s | mail -s \"Bitcoin Alert\"" \
            "admin@foo.com"
        echo
        exit 1
    fi

    mkdir -p "$BETHD_PIDDIR"
    chmod 0755 "$BETHD_PIDDIR"
    chown $BETHD_USER:$BETHD_GROUP "$BETHD_PIDDIR"
    chown $BETHD_USER:$BETHD_GROUP "$BETHD_CONFIGFILE"
    chmod 0660 "$BETHD_CONFIGFILE"
end script

exec start-stop-daemon \
    --start \
    --pidfile "$BETHD_PIDFILE" \
    --chuid $BETHD_USER:$BETHD_GROUP \
    --exec "$BETHD_BIN" \
    -- \
    -pid="$BETHD_PIDFILE" \
    -conf="$BETHD_CONFIGFILE" \
    -datadir="$BETHD_DATADIR" \
    -disablewallet \
    -daemon

