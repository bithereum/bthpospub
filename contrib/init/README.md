Sample configuration files for:
```
SystemD: bethd.service
Upstart: bethd.conf
OpenRC:  bethd.openrc
         bethd.openrcconf
CentOS:  bethd.init
OS X:    org.bitcoin.bethd.plist
```
have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
